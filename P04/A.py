import numpy as np

def calc_poly(points):
    num_points = points.shape[1];

    def n_func(i, x):
        ret = 1.0;
        for j in range(i):
            ret *= (x - points[0, j]);
        return ret;

    n_mat = np.zeros((num_points, num_points));
    for i in range(num_points):
        for j in range(i + 1):
            n_mat[i, j] = n_func(j, points[0, i]);

    c_mat = np.linalg.solve(n_mat, points[1, :]);

    def poly(x): 
        y = 0;
        for i in range(num_points):
            y += c_mat[i] * n_func(i, x);
        return y;
    return poly;

def plot(points, poly, func = None):
    from matplotlib import pyplot as plt;
    plt.xlabel("x");
    plt.ylabel("y");

    min_x = np.min(points[0]);
    max_x = np.max(points[0]);
    ep = (max_x - min_x) / (10 * points.shape[1])
    x = np.arange(min_x, max_x + ep, ep);

    plt.plot(points[0], points[1], 'o', label="Points");
    plt.plot(x, poly(x), label="Polynomial");
    if func != None:
        plt.plot(x, func(x), label="Function");
    plt.legend();
    plt.show();

if __name__ == "__main__":
    # p = np.array([
    #     [-1, 0,  1],
    #     [ 2, 4, 12],
    # ]);
    p = np.array([
        [ -3, -2, -1,  0,  1,  2,  3 ],
        [ -3,  4, -2,  0, -2,  4,  3 ],
    ]);
    f = calc_poly(p);
    plot(p, f);
