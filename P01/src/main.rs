use nalgebra as na;
use std::{
    fmt::Display,
    io::{BufRead, Stdin},
};
fn main() {
    let args = std::env::args();
    let mut sys: LinearSystem;
    if args.len() > 1 {
        sys = LinearSystem::from(&args.into_iter().skip(1).collect::<Vec<String>>());
    } else {
        println!("Enter linear system :");
        sys = LinearSystem::from(&std::io::stdin());
    }
    println!("Solving System :\n{}", &sys);
    sys.solve();
    if sys.is_result_exact() {
        let x = sys.get_vars();
        println!("\nExact Results : ");
        for i in 0..x.len() {
            println!("x{} = {}", i + 1, x[i]);
        }
        println!("\nErrors :");
        for eq in &sys.eqs {
            println!("{} => {}", eq, eq.substitute(&x));
        }
    } else if sys.is_result_valid() {
        let x = sys.get_vars();
        println!("\nBest Possible Results : ");
        for i in 0..x.len() {
            println!("x{} = {}", i + 1, x[i]);
        }
        println!("\nErrors :");
        for eq in &sys.eqs {
            println!("{} => {}", eq, eq.substitute(&x));
        }
    } else {
        println!("\nNo solution");
    }
}

#[derive(Debug)]
struct LinearEquation {
    a: Vec<f64>,
    b: f64,
}
impl LinearEquation {
    pub fn substitute(&self, x: &[f64]) -> f64 {
        let c = x.len();
        if c != self.a.len() {
            panic!("Invalid substitution");
        }
        let mut ret = 0.0;
        for i in 0..c {
            ret += x[i] * self.a[i];
        }
        ret += self.b;
        return ret;
    }
}
#[derive(Debug)]
struct LinearSystem {
    eqs: Vec<LinearEquation>,
    var: Vec<f64>,
}
impl LinearSystem {
    pub fn solve(&mut self) {
        if self.eqs.len() == self.var.len() {
            match Self::exec_gauss_jordan(self.as_matrix()) {
                Err(err) => println!("{}", err),
                Ok(ans) => {
                    if ans.len() != self.var.len() {
                        panic!(
                            "Invalid answer count; Expected {}, Found {}",
                            self.var.len(),
                            ans.len()
                        );
                    } else {
                        self.var = ans;
                    }
                }
            };
        } else {
            let a = self.get_mults_matrix();
            let a_t = a.transpose();
            let new_mults: na::DMatrix<f64> = &a_t * a;
            let new_vals: Vec<f64> = (a_t * self.get_vals_matrix()).iter().map(|x| *x).collect();
            let mut square_system =
                <LinearSystem as From<(&na::DMatrix<f64>, &[f64])>>::from((&new_mults, &new_vals));
            square_system.solve();
            self.var = square_system.var;
        }
    }
    pub fn is_result_valid(&self) -> bool {
        for x in self.var.iter() {
            if x.is_nan() {
                return false;
            }
        }
        return true;
    }
    pub fn is_result_exact(&self) -> bool {
        if !self.is_result_valid() {
            return false;
        }
        for eq in &self.eqs {
            if eq.substitute(&self.var).abs() > f64::EPSILON {
                return false;
            }
        }
        return true;
    }
    pub fn get_vars(&self) -> &[f64] {
        return &self.var;
    }
    pub fn as_matrix(&self) -> na::DMatrix<f64> {
        let eq_count = self.eqs.len();
        let var_count = self.var.len();
        let col_count = var_count + 1;
        let mut a = Vec::<f64>::with_capacity(eq_count * col_count);
        for r in 0..eq_count {
            for c in 0..var_count {
                a.push(self.eqs[r].a[c]);
            }
            a.push(self.eqs[r].b);
        }
        return na::DMatrix::<f64>::from_row_slice(eq_count, col_count, &a);
    }
    pub fn get_mults_matrix(&self) -> na::DMatrix<f64> {
        let eq_count = self.eqs.len();
        let var_count = self.var.len();
        let mut a = Vec::<f64>::with_capacity(eq_count * var_count);
        println!("rc: {}, cc: {}", eq_count, var_count);
        for r in 0..eq_count {
            for c in 0..var_count {
                a.push(self.eqs[r].a[c]);
            }
        }
        return na::DMatrix::<f64>::from_row_slice(eq_count, var_count, &a);
    }
    pub fn get_vals_matrix(&self) -> na::DMatrix<f64> {
        let eq_count = self.eqs.len();
        let mut b = Vec::<f64>::with_capacity(eq_count);
        for r in 0..eq_count {
            b.push(self.eqs[r].b);
        }
        return na::DMatrix::<f64>::from_row_slice(eq_count, 1, &b);
    }
    fn exec_gauss_jordan(mut mat: na::DMatrix<f64>) -> Result<Vec<f64>, &'static str> {
        let n = mat.nrows();
        if n != (mat.ncols() - 1) {
            panic!("Number of equations and variables must be equal");
        }
        // Gauss Jordan elimination
        for i in 0..n {
            if mat[(i, i)] == 0f64 {
                for j in i..n {
                    if mat[(j, i)] != 0f64 {
                        mat.swap_rows(i, j);
                        break;
                    }
                }
            }
            for j in 0..n {
                if i != j {
                    let ratio = mat[(j, i)] / mat[(i, i)];
                    for k in 0..(n + 1) {
                        mat[(j, k)] = mat[(j, k)] - ratio * mat[(i, k)];
                    }
                }
            }
        }
        // Simply divide the last column by diagonals instead of back substitution
        let mut ret = vec![0f64; n];
        for i in 0..n {
            ret[i] = -mat[(i, n)] / mat[(i, i)];
        }
        return Ok(ret);
    }
}

impl Display for LinearEquation {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut result = String::new();
        for i in 0..self.a.len() {
            if i == 0 {
                if self.a[i] < 0.0 {
                    result.push_str(&format!("- {} X1", -self.a[i]));
                } else {
                    result.push_str(&format!("+ {} X1", self.a[i]));
                }
            } else if self.a[i] < 0.0 {
                result.push_str(&format!(" - {} X{}", -self.a[i], i + 1));
            } else {
                result.push_str(&format!(" + {} X{}", self.a[i], i + 1));
            }
        }
        if self.b < 0.0 {
            result.push_str(&format!(" - {} = 0", -self.b));
        } else {
            result.push_str(&format!(" + {} = 0", self.b));
        }
        return write!(f, "{}", result.trim());
    }
}
impl Display for LinearSystem {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut result = String::new();
        for i in 0..self.eqs.len() {
            if i > 0 {
                result.push_str("\n");
            }
            result.push_str(&format!("{}", self.eqs[i]));
        }
        return write!(f, "{}", result);
    }
}
impl From<(&na::DMatrix<f64>, &[f64])> for LinearSystem {
    fn from(mults_and_vals: (&na::DMatrix<f64>, &[f64])) -> Self {
        let (mults, vals) = mults_and_vals;
        println!("NM : {}x{}", mults.nrows(), mults.ncols());
        println!("NV : {}", vals.len());
        if mults.nrows() != vals.len() {
            panic!(
                "Linear System equation count and value count are not equal. Equation Count : '{}', Value Count : '{}'",
                mults.nrows(),
                vals.len()
            );
        }
        let eq_count = mults.nrows();
        let mut eqs = Vec::<LinearEquation>::with_capacity(eq_count);
        for i in 0..eq_count {
            let a = mults.row(i).iter().map(|v| *v).collect();
            let b = vals[i];
            eqs.push(LinearEquation { a, b });
        }
        return Self {
            eqs,
            var: vec![f64::NAN; mults.ncols()],
        };
    }
}
impl From<&Stdin> for LinearSystem {
    fn from(stdin: &Stdin) -> LinearSystem {
        let mut lines = stdin.lock().lines();
        let (eqs_count, var_count) = {
            let line = lines.next().unwrap().unwrap();
            let mut line = line.split_whitespace();
            let n = line.next().unwrap().parse::<usize>().unwrap();
            let m = line.next().unwrap().parse::<usize>().unwrap();
            (n, m)
        };
        let mut eqs = Vec::<LinearEquation>::with_capacity(eqs_count);
        for i_eq in 0..eqs_count {
            let line = lines.next().unwrap().unwrap();
            let mut line = line.split_whitespace();
            eqs.push(LinearEquation {
                a: vec![0f64; var_count],
                b: 0f64,
            });
            for i_var in 0..var_count {
                eqs[i_eq].a[i_var] = line
                    .next()
                    .expect(
                        format!(
                            "Invalid number of values for linear equation. Expected {} per line",
                            var_count + 1
                        )
                        .as_str(),
                    )
                    .parse::<f64>()
                    .unwrap();
            }
            eqs[i_eq].b = line
                .next()
                .expect("One more value (for 'b') required")
                .parse::<f64>()
                .unwrap();
        }
        return LinearSystem {
            eqs,
            var: vec![f64::NAN; var_count],
        };
    }
}
impl From<&Vec<String>> for LinearSystem {
    fn from(args: &Vec<String>) -> Self {
        let mut eqs = Vec::new();
        if args.len() < 2 {
            panic!("Invalid number of arguments");
        }
        let eqs_count = args[0].parse::<usize>().unwrap();
        let var_count = args[1].parse::<usize>().unwrap();
        if args.len() != eqs_count * (var_count + 1) + 2 {
            panic!(
                "Invalid number of arguments; Expected {}, Found {}",
                eqs_count * (var_count + 1) + 2,
                args.len()
            );
        }
        let mut a = Vec::with_capacity(var_count);

        for i_eq in 0..eqs_count {
            a.clear();
            for i_var in 0..var_count {
                a.push(
                    args[2 + i_eq * (var_count + 1) + i_var]
                        .parse::<f64>()
                        .unwrap(),
                );
            }
            let leq = LinearEquation {
                a: a.clone(),
                b: -args[2 + i_eq * (var_count + 1) + var_count]
                    .parse::<f64>()
                    .unwrap(),
            };
            eqs.push(leq);
        }
        return LinearSystem {
            eqs,
            var: vec![f64::NAN; var_count],
        };
    }
}
