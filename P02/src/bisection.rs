pub fn run(target: fn(f64) -> f64, range: (f64, f64), tolerance: f64) -> Result<f64, &'static str> {
    let fa = target(range.0);
    let fb = target(range.1);
    if fa * fb == 0.0 {
        return Ok(if fa == 0.0 { range.0 } else { range.1 });
    } else if fa * fb > 0.0 {
        return Err("f(a) and f(b) have the same sign");
    }
    return Ok(execute(target, range.0, range.1, tolerance));
}

fn execute(f: fn(f64) -> f64, mut a: f64, mut b: f64, tolerance: f64) -> f64 {
    let mut c = (a + b) / 2.0;
    let mut stop_condition = f64::INFINITY;
    while stop_condition > tolerance {
        let fc = f(c);
        if fc == 0.0 {
            return c;
        }
        else if f(a) * fc < 0.0 {
            b = c;
        } else {
            a = c;
        }
        let c_last = c;
        c = (a + b) / 2.0;
        stop_condition = (c - c_last).abs() / c.abs();
    }
    return c;
}
